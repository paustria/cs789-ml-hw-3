'''
    CS789: Advance Machine Learning: Assignment #3

    In this assignment, I implement Federated Averaging used in Federated
    Learning.

    main_fl.py is the driver program that creates clients and uses the
    federated averaging algorithm. The test in this program is to find out how many
    communication rounds it takes for the global model to reach 97% accuracy.
'''

import random
import tensorflow as tf
import federated_averaging as fa
import my_utils as u

from sklearn.preprocessing import LabelBinarizer
from tensorflow.keras.optimizers import SGD
from tensorflow.keras import backend as K

### constants
MAX_ROUNDS = 2000
NUM_CLIENTS = 100
NUM_CLIENTS_PER_ROUND = 1
TARGET_ACC = 0.97

# model information
SHAPE = (28, 28)
BATCH_SIZE = 10 # infinity = fedSGD
EPOCHS = 1

# mnist catagories 0-9
CLASSES = 10

# stochastic gradient variables
LEARNING_RATE = 0.03
LOSS = 'categorical_crossentropy'
METRICS = ['accuracy']
MOMENTUM = 0.0  # default 0.0
### end constants

# import MNIST dataset
mnist = tf.keras.datasets.mnist

# load training and testing
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# normalize pixel values from [0, 1]
x_train, x_test = x_train / 255.0, x_test / 255.0

# binarize the labels
lb = LabelBinarizer()
y_train = lb.fit_transform(y_train)
y_test = lb.fit_transform(y_test)

# create clients
clients = u.create_clients(x_train,
                           y_train,
                           num_clients=NUM_CLIENTS,
                           initial='client')

# process and batch the training data for each client
clients_batched = dict()
for (client_name, data) in clients.items():
    clients_batched[client_name] = u.batch_data(data, BATCH_SIZE)

# process and batch the test set
test_batched = tf.data.Dataset.from_tensor_slices(
    (x_test, y_test)).batch(len(y_test))

# create optimizer
optimizer = SGD(lr=LEARNING_RATE, momentum=MOMENTUM)

# create the global model
smlp_global = u.SimpleMLP()
global_model = smlp_global.build(SHAPE, CLASSES)
global_acc = 0.0

# print test information
print('starting test...')
print('total clients: {}'.format(NUM_CLIENTS))
print('clients selcted per round: {}'.format(NUM_CLIENTS_PER_ROUND))
print('clients training epochs: {}'.format(EPOCHS))
print('clients training batch size: {}'.format(BATCH_SIZE))
print('max communication rounds: {}'.format(MAX_ROUNDS))

# commence global training loop
for comm_round in range(MAX_ROUNDS):
    if global_acc >= TARGET_ACC:
        print('reached target accuracy, terminating')
        break

    # get the global model's weights to give to local models
    global_weights = global_model.get_weights()

    # list to collect local model weights after scaling
    all_scaled_local_weights = list()

    # randomize client data - using keys
    client_names = list(clients_batched.keys())
    random.shuffle(client_names)

    # pick clients for local model training
    selected_clients = client_names[0:NUM_CLIENTS_PER_ROUND]

    # loop through each client and create new local model
    for client in selected_clients:
        #print('client selected:' , client)
        smlp_local = u.SimpleMLP()
        local_model = smlp_local.build(SHAPE, CLASSES)
        local_model.compile(loss=LOSS, optimizer=optimizer, metrics=METRICS)

        # set local model weight to the weight of the global model
        local_model.set_weights(global_weights)

        # fit local model with client's training data
        local_model.fit(clients_batched[client], epochs=EPOCHS, verbose=0)

        # scale the model weights and add to list
        scaling_factor = fa.local_to_global_sample_ratio(clients_batched, client,
                                                   selected_clients)
        scaled_weights = fa.scale_local_weights(local_model.get_weights(),
                                                scaling_factor)
        all_scaled_local_weights.append(scaled_weights)

        # free memory after each round
        K.clear_session()

    # sum of the scaled local models' weights
    sum_local_models = fa.sum_scaled_weights(all_scaled_local_weights)

    # update global model
    global_model.set_weights(sum_local_models)

    # test global model and print out metrics after each communications round
    for (X_test, Y_test) in test_batched:
        global_acc, global_loss = u.test_model(X_test, Y_test, global_model,
                                               comm_round)
