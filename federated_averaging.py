'''
    CS789: Advance Machine Learning - Assignment #3

    federated_averaging.py contains the functions to implement the 
    federated averaging algorithm from the paper titled
    "Communication-Efficient Learning of Deep Networks from Decentrazlied Data"
'''
import tensorflow as tf


def local_to_global_sample_ratio(clients_trn_data, client_name,
                           selected_clients_names):
    '''
        Calculates the scaling factor (n_k/n). 
        The scaling factor is multiplied with local update before summation.

        returns:
            individual client data scaling factor (n_k/n)

        args:
            clients_trn_data - dictionary of all clients training data
            client_name - name of client
            selected_clients_names - list of selected clients' names
    '''

    batch_size = list(clients_trn_data[client_name])[0][0].shape[0]

    # get the total training data points across selected clients
    global_count = sum([
        tf.data.experimental.cardinality(
            clients_trn_data[client_name]).numpy()
        for client_name in selected_clients_names
    ]) * batch_size

    # get the total number of data points held by a client
    local_count = tf.data.experimental.cardinality(
        clients_trn_data[client_name]).numpy() * batch_size

    return local_count / global_count


def scale_local_weights(weights, scalar):
    '''
        Function for scaling a models weights.
        The weights from the local model are multiplied by the scaling factor (n_k/n).

        returns:
            list of products from multiplying the weights and scale value

        args:
            weights - individual client's model weights
            scalar - a client's data scalar value (n_k/n)
    '''

    weight_final = []
    steps = len(weights)

    for i in range(steps):
        weight_final.append(scalar * weights[i])

    return weight_final


def sum_scaled_weights(scaled_weights):
    '''
        Sums the local model's scaled weights.
        This is equilavent to the average of local model.

        returns:
            sum of the local models scaled weights

        args:
            scaled_weights - list of local models scaled weights
    '''

    sum_weights = list()
    for grad_list_tuple in zip(*scaled_weights):
        layer_sum = tf.math.reduce_sum(grad_list_tuple, axis=0)
        sum_weights.append(layer_sum)

    return sum_weights
