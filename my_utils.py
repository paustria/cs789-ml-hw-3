'''
    This files contains functions to do the following:
    1) created clients for federated learning
    2) create a simple multi-layer perceptron network
    3) peform Federated Averaging algorithm
'''

import numpy as np
import random
from imutils import paths
from sklearn.model_selection import train_test_split
from sklearn.utils import shuffle
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelBinarizer

import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv2D
from tensorflow.keras.layers import Reshape
from tensorflow.keras.layers import MaxPooling2D
from tensorflow.keras.layers import Activation
from tensorflow.keras.layers import Flatten
from tensorflow.keras.layers import Dense


def create_non_iid_clients(train_images,
                           train_labels,
                           digits=1,
                           num_clients=10):
    '''Creates non iid clients. TESTING'''

    non_iid_clients = dict()

    unique_labels = np.unique(np.array(train_labels))
    random.shuffle(unique_labels)

    # create the group of digits
    sub_label_list = [
        unique_labels[i:i + digits]
        for i in range(0, len(unique_labels), digits)
    ]

    # how many clients to create for each digit-group
    num_clients_per_group = int(num_clients / (len(unique_labels) / digits))
    # label binarizer
    lb = LabelBinarizer()
    lb.fit(range(10))

    # create client to hold digits only in the digit group
    for label_group in sub_label_list:
        # group data by digit in digit-group
        class_data = [(image, label)
                      for (image, label) in zip(train_images, train_labels)
                      if label in label_group]

        # destruct tuple
        images, labels = zip(*class_data)
        labels = lb.transform(labels)

        name_prefix = 'client_{}'.format(str(label_group))

        # create clients with non-iid data
        clients = create_clients(list(images), list(labels),
                                 num_clients_per_group, name_prefix)

        non_iid_clients.update(clients)

    return non_iid_clients


def create_clients(images, labels, num_clients=10, initial='clients'):
    '''
        Creates clients with i.i.d dataset.

        args:
            images: a list of numpy arrays of training images
            labels: a list of binarized labels for each image
            num_client: number of fedrated members (clients)
            initials: the clients'name prefix, e.g, clients_1

        return:
            a dictionary (k, v) where
                k = clients' names
                v = data shards containing list of tuples (image, label)
    '''

    # create a list of client names
    client_names = ['{}_{}'.format(initial, i + 1) for i in range(num_clients)]

    # randomize the data
    data = list(zip(images, labels))
    random.shuffle(data)

    # shard data and place at each client
    size = len(data) // num_clients
    shards = [data[i:i + size] for i in range(0, size * num_clients, size)]

    # number of clients must equal number of shards
    assert len(shards) == len(client_names)

    # dictionary of client names and their data
    return {client_names[i]: shards[i] for i in range(len(client_names))}


def batch_data(data_shard, batch_size=10):
    '''
        Takes in a data shard and creates a tf dataset object.

        args:
            data_shard: list of tuples (image, label)
            bs: batch size

        return:
            tf dataset object
    '''

    #seperate shard into data and labels lists
    data, label = zip(*data_shard)
    dataset = tf.data.Dataset.from_tensor_slices((list(data), list(label)))

    if batch_size == 'infinity':
        return dataset.shuffle(len(label)).batch(len(label))

    return dataset.shuffle(len(label)).batch(batch_size)

class SimpleMLP:
    '''
        Creates a simple 2 layer perceptron network.
        The input is a  (28,28) flatten array.
    '''
    @staticmethod
    def build(shape, classes):
        '''Creates a simple MLP '''

        model = Sequential()
        model.add(Flatten(input_shape=shape))
        model.add(Dense(200))
        model.add(Activation("relu"))
        model.add(Dense(200))
        model.add(Activation("relu"))
        model.add(Dense(classes))
        model.add(Activation("softmax"))
        return model


class SimpleCNN:
    '''
        Creates a simple 2 covulational neural network.
        The input is a  (28,28) flatten array.
    '''
    @staticmethod
    def build(shape, classes):
        '''Creates a simple MLP '''

        model = Sequential()
        model.add(
            Conv2D(32,
                   kernel_size=(5, 5),
                   activation='relu',
                   kernel_initializer='he_uniform',
                   input_shape=shape))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Conv2D(64, kernel_size=(5, 5), activation='relu'))
        model.add(MaxPooling2D(pool_size=(2, 2)))
        model.add(Flatten())
        model.add(Dense(512))
        model.add(Activation('relu'))
        model.add(Dense(classes))
        model.add(Activation('softmax'))
        return model


def test_model(x_test, y_test, model, comm_round):
    '''Test model and print report'''

    cce = tf.keras.losses.CategoricalCrossentropy(from_logits=True)
    logits = model.predict(x_test)
    loss = cce(y_test, logits)
    acc = accuracy_score(tf.argmax(logits, axis=1), tf.argmax(y_test, axis=1))
    print('comm_round: {} | global_acc: {:.3%} | global_loss: {}'.format(
        comm_round, acc, loss))
    return acc, loss
