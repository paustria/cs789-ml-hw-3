# CS789 Advance Machine Learning: Homework #3

## Federated Averaging Implmentation

In this assignment, I implemented the Federated Averaging algorithm described in [1]. The algorithm can be found in the _federeated_averaging.py_ file. The _main_fl.py_ file executes executes a test to find out many rounds it takes for the gloabl model to reach 97%.

[1] H. Brendan McMahan, E. Moore, D. Ramage, S. Hampson, and B. Agüera y Arcas, “Communication-efficient learning of deep networks from decentralized data,” Proc. 20th Int. Conf. Artif. Intell. Stat. AISTATS 2017, vol. 54, 2017.

### Test Description

- MNIST dataset
- 100 clients
- model - 2-layer multilayer perceptron, batch size = 10, epoch = 1
- data distributed in a i.i.d. fashion
- objective - how many communication rounds the global model takes to reach **97% accuracy** varying the amount of clients 

### Pre-Requisites

[Pipenv](https://pypi.org/project/pipenv) needs to be installed in order to run the program

### Running main_fl.py

1. run `git clone https://gitlab.com/paustria/cs789-ml-hw-3.git && cd cs789-ml-hw-3`
2. run `pipenv install`
3. run `pipenv shell`
4. set constants in main_fl.py
5. run `python main_fl.py` to run test