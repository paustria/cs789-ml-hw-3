'''
CS789: Advance Machine Learning: Assignment #3

main_sgd.py uses Federated Averaging with a single client with the entire
dataset. This was created just for comparison.
'''

import random
from sklearn.preprocessing import LabelBinarizer

import tensorflow as tf
from tensorflow.keras.optimizers import SGD
from tensorflow.keras import backend as K

# Federated Learning functions
from main_utils import *

### constants
# model information
SHAPE = (28, 28)

# mnist catagories 0-9
CLASSES = 10

# stochastic gradient variables
LEARNING_RATE = 0.01
LOSS = 'categorical_crossentropy'
METRICS = ['accuracy']
MOMENTUM = 0.9
ROUNDS = 1000
### end constants

# import mnist dataset
mnist = tf.keras.datasets.mnist

# load training and testing
(x_train, y_train), (x_test, y_test) = mnist.load_data()

# normalize pixel values from [0, 1]
x_train, x_test = x_train / 255.0, x_test / 255.0

# binarize the labels
lb = LabelBinarizer()
y_train = lb.fit_transform(y_train)
y_test = lb.fit_transform(y_test)

# process and batch the test set
test_batched = tf.data.Dataset.from_tensor_slices(
    (x_test, y_test)).batch(len(y_test))

# create optimizer
optimizer = SGD(lr=LEARNING_RATE, momentum=MOMENTUM)

# create the global model
smlp_global = SimpleMLP()
global_model = smlp_global.build(SHAPE, CLASSES)

SGD_dataset = tf.data.Dataset.from_tensor_slices(
    (x_train, y_train)).shuffle(len(y_train)).batch(len(y_train))

smlp_SGD = SimpleMLP()
SGD_model = smlp_SGD.build(SHAPE, CLASSES)

SGD_model.compile(loss=LOSS, optimizer=optimizer, metrics=METRICS)

# fit the SGD training data to model
_ = SGD_model.fit(SGD_dataset, epochs=ROUNDS, verbose=0)

#test the SGD global model and print out metrics
for (X_test, Y_test) in test_batched:
    SGD_acc, SGD_loss = test_model(X_test, Y_test, SGD_model, 1)
